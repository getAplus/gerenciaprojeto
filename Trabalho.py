def DaSplitLinha(conteudo):
    return conteudo.split('\n')


def DaSplitEspaco(conteudo):
    return conteudo.split(' ')


def Formatar(lista):
    if lista[-1] in ['Junior','Sobrinho','Neto','Filho']:
        fim=len(lista)-2
        print(lista[-2].upper() + ' ' + lista[-1].upper()+',',end=' ')

    else:
        fim=len(lista)-1
        print(lista[-1].upper()+',',end=' ')

    for i in range (fim):
        if lista[i] not in ['da','de','do','das','dos','des']:
            print(lista[i][0].upper()+'.',end=' ')
        else:
            print(lista[i],end=' ')
    print('\n')


def LerArquivo(NomeArquivo):
    arq=open(NomeArquivo,'r')
    conteudo=arq.read()
    lista=DaSplitLinha(conteudo)
    #print(lista)
    
    for i in lista:
        l = DaSplitEspaco(i)
        #print(l)
        Formatar(l)


def VirarMaiuscula(palavra):
    return palavra.upper()


def PrimeiraLetraMaiuscula(palavra):
    return palavra.capitalize()


LerArquivo('nome.txt')
